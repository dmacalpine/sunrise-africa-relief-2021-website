const path = require( 'path' );
const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const OptimizeCSSAssetsPlugin = require( 'optimize-css-assets-webpack-plugin' );
const CopyPlugin = require( 'copy-webpack-plugin' );

module.exports = {
	mode: 'development',
	devtool: 'source-map',
	entry: './web/app/themes/sar/src/js/index.js',
	output: {
		path: path.resolve( __dirname, 'web/app/themes/sar/dist' ),
		filename: 'bundle.js',
		publicPath: './dist/',
	},

	plugins: [
		new MiniCssExtractPlugin( { filename: '../style.css' } ),
		new BrowserSyncPlugin( {
			files: [
				'**/*.php',
				'**/src/**/*',
				'**/templates/**/*',
				'tailwind.config.js',
			],
			injectChanges: true,
			proxy: 'http://sunriseafrica.test',
			injectCss: true,
			notify: false,
			open: false,
		} ),

		// Minify CSS assets
		new OptimizeCSSAssetsPlugin( {} ),
		new CopyPlugin( [
			{
				from: 'web/app/themes/sar/src/images/*.*',
				to: 'images/',
				flatten: true,
			},
		] ),
	],

	module: {
		rules: [
			{
				// Extract any SCSS content and minimize
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					{
						loader: 'postcss-loader',
					},
					{
						loader: 'sass-loader',
						options: {},
					},
				],
			},
			{
				// Extract any CSS content and minimize
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					{ loader: 'postcss-loader' },
				],
			},
		],
	},
};
